<?php
declare(strict_types=1);

namespace App\Template;

use App\AppParametersTrait;
use App\Localization\TranslatorAwareInterface;
use App\Localization\TranslatorAwareTrait;
use App\Macros\AssetMacro;
use Latte\Engine as LatteEngine;
use Nette\Application\UI\Control;
use Nette\Application\UI\Template;
use Nette\Bridges\ApplicationLatte\Template as LatteTemplate;
use Nette\Bridges\ApplicationLatte\TemplateFactory as TemplateFactoryParent;

/**
 * Application factory for templates
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
class TemplateFactory extends TemplateFactoryParent implements TranslatorAwareInterface
{

    use AppParametersTrait;
    use TranslatorAwareTrait;

    public function createTemplate(Control $control = null, string $class = null): Template
    {
        $template = parent::createTemplate($control, $class);
        if (isset($this->translator) && $template instanceof LatteTemplate) {
            $template->setTranslator($this->translator);
        }

        $template->getLatte()->onCompile[] = function (LatteEngine $latte) use ($template) {
            (new AssetMacro($latte->getCompiler(), $this->appParameters['wwwDir'], $template->basePath))->install();
        };

        return $template;
    }
}