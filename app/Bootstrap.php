<?php
declare(strict_types=1);

namespace App;

use Dotenv\Dotenv;
use Nette\Configurator;
use Tester\Environment;

/**
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class Bootstrap
{

    public static function boot(): Configurator
    {
        $appDir = __DIR__;

        $dotenv = Dotenv::createImmutable($appDir.'/..');
        $dotenv->safeLoad();

        $configurator = new Configurator;

        $configurator->setDebugMode(boolval($_ENV['DEBUG'] ?? false));
        $configurator->enableTracy($appDir.'/../log');

        $configurator->setTimeZone('Europe/Prague');
        $configurator->setTempDirectory($appDir.'/../temp');

        $configurator->addConfig($appDir.'/config/config.neon');
        $localConfigFile = $appDir.'/config/local.neon';
        if (file_exists($localConfigFile)) {
            $configurator->addConfig($localConfigFile);
        }

        return $configurator;
    }
}