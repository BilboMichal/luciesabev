<?php
declare(strict_types=1);

namespace App;

/**
 * Setter for global application parameters
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
trait AppParametersTrait
{
    protected array $appParameters = [];

    public function setAppParameters(array $appParameters)
    {
        $this->appParameters = $appParameters;
        return $this;
    }
}