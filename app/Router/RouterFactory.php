<?php
declare(strict_types=1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\RouteList;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class RouterFactory
{

    use Nette\StaticClass;

    public static function createRouter(string $defaultLang): RouteList
    {
        $langMask = '[<lang [a-z]{2}>/]';

        $router = new RouteList;
        $router->addRoute($langMask . '[c<id [0-9]+>[-<urltitle>]]', [
            'presenter' => 'Homepage',
            'action' => 'category',
            'lang' => $defaultLang,
        ]);
        $router->addRoute($langMask . '<presenter>/<action>', [
            'presenter' => 'Homepage',
            'action' => 'default',
            'lang' => $defaultLang,
        ]);

        return $router;
    }
}