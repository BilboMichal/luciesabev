<?php
declare(strict_types=1);

namespace App\Forms;

use App\Localization\TranslatorAwareTrait;
use App\Localization\TranslatorAwareInterface;
use Nette\Application\UI\Form;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class FormFactory implements TranslatorAwareInterface
{

    use TranslatorAwareTrait;

    public function create(): Form
    {
        $form = new Form;
        if (isset($this->translator)) {
            $form->setTranslator($this->translator);
        }

        return $form;
    }
}