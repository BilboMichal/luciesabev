<?php
declare(strict_types=1);

namespace App\Macros;

use Latte\CompileException;
use Latte\Compiler;
use Latte\MacroNode;
use Latte\Macros\MacroSet;
use Latte\PhpWriter;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class AssetMacro extends MacroSet
{

    /**
     *
     * @param Compiler $compiler
     * @param string $wwwDir
     * @param string $basePath
     * @param bool $tryMinFile Try <asset>.min.<ext> analogous path to asset file if present
     */
    public function __construct(Compiler $compiler, private string $wwwDir, private string $basePath,
                                private bool $tryMinFile = false)
    {
        parent::__construct($compiler);
    }

    private function buildAssetPath(string $path): string
    {
        $extPathPrefs = ['http://', 'https://', '//'];
        $isPathExt = false;
        foreach ($extPathPrefs as $extPathPref) {
            if (str_starts_with($path, $extPathPref)) {
                $isPathExt = true;
                break;
            }
        }
        $isPathAbsolute = str_starts_with($path, '/');

        // Build min path if possible...
        $pathinfo = pathinfo($path);
        if (!$isPathExt && $this->tryMinFile) {
            $minPath = !str_ends_with($pathinfo[PATHINFO_FILENAME], '.min') ?
                $pathinfo[PATHINFO_DIRNAME].'/'.$pathinfo[PATHINFO_FILENAME].'.min.'.$pathinfo[PATHINFO_EXTENSION] : $path;
        }

        if (isset($minPath) && file_exists($this->wwwDir.'/'.$minPath)) {
            $path = $minPath;
        }

        $version = file_exists($this->wwwDir.'/'.$path) ? filemtime($this->wwwDir.'/'.$path) : null;

        return (!$isPathAbsolute && !$isPathExt ? $this->basePath.'/' : '').$path.'?v='.$version;
    }

    public function install()
    {
        $this->addMacro('asset', null, null,
            function (MacroNode $node, PhpWriter $writer): string {
                $path = $node->args;
                $htmlNode = $node->htmlNode;
                $htmlAttrs = $htmlNode->attrs;
                $renderAttrs = [];

                if ($htmlNode->name === 'link') {
                    if (!isset($htmlAttrs['href'])) {
                        $renderAttrs['href'] = $this->buildAssetPath($path);
                    }
                    if (!isset($htmlAttrs['type'])) {
                        if (str_contains($path, '.css')) {
                            $renderAttrs['type'] = 'text/css';
                        }
                    }
                    if (!isset($htmlAttrs['rel'])) {
                        if (str_contains($path, '.css')) {
                            $renderAttrs['rel'] = 'stylesheet';
                        }
                        if (str_contains($path, '.ico')) {
                            $renderAttrs['rel'] = 'shortcut icon';
                        }
                    }
                } elseif ($htmlNode->name === 'script') {
                    if (!isset($htmlAttrs['src'])) {
                        $renderAttrs['src'] = $this->buildAssetPath($path);
                    }
                    if (!isset($htmlAttrs['type'])) {
                        if (str_contains($path, '.js')) {
                            $renderAttrs['type'] = 'text/javascript';
                        }
                    }
                } else {
                    throw new CompileException('n:asset macro must be used on <link> or <script> tags only');
                }

                $render = '';
                foreach ($renderAttrs as $attr => $value) {
                    $render .= ' '.$attr.'="'.htmlspecialchars($value, HTML_ENTITIES).'"';
                }

                return $writer->write('echo \''.rtrim($render).'\';');
            });
    }
}