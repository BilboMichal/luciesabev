<?php
declare(strict_types=1);

namespace App\Localization;

use Dibi\Connection;
use Nette\Application\UI\Presenter;
use Nette\Localization\Translator as NetteTranslatorInterface;
use Stichoza\GoogleTranslate\GoogleTranslate;

/**
 * Simple application translator through DB
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class Translator implements NetteTranslatorInterface
{
    private string $lang;

    /** @internal key => entry */
    private object $contentPairs;

    public function __construct(private Connection $connection, private string $defaultLang, private array $langs)
    {
        $this->lang = $defaultLang;
    }

    public function getContentPairs(): object
    {
        if (!isset($this->contentPairs)) {
            $contentRows = $this->connection->select('*')
                ->from('content_locale');

            $contentPairs = [];
            foreach ($contentRows as $contentRow) {
                $contentPairs[$contentRow->key] = $contentRow;
            }

            $this->contentPairs = (object) $contentPairs;
        }

        return $this->contentPairs;
    }

    public function getLang(): string
    {
        return $this->lang;
    }

    public function setLang(string $lang): self
    {
        $this->lang = $lang;
        return $this;
    }

    private static function translateMessage(string $langSource, string $langTarget, string $message): string
    {
        return GoogleTranslate::trans($message, $langTarget, $langSource);
    }

    public function translate($message, ...$parameters): string
    {
        $content = $this->getContentPairs();

        if (!isset($content->$message)) {
            $content->$message = (object) [
                    'key' => $message,
                    'value_'.$this->defaultLang => $message,
                    'update' => true,
            ];
        }

        // Chybí-li zpráva pro daný jazyk
        if (!isset($content->$message->{'value_'.$this->lang})) {
            // Je-li výchozí jazyk - doplníme výchozí jazyk ze zprávy
            if ($this->lang === $this->defaultLang) {
                $content->$message->{'value_'.$this->lang} = $message;
                $content->$message->update = true;
            }
            // Nevýchozí jazyk --> překlad z výchozího jazyka (pokud je dostupný)
            if ($this->lang !== $this->defaultLang && isset($content->$message->{'value_'.$this->defaultLang})) {
                $content->$message->{'value_'.$this->lang} = self::translateMessage($this->defaultLang, $this->lang,
                        $content->$message->{'value_'.$this->defaultLang});
                $content->$message->update = true;
            }
        }

        return $content->$message->{'value_'.$this->lang} ?? (string) $message;
    }

    public function updateTranslations()
    {
        $content = $this->getContentPairs();

        // Aktualizovat vše označené příznakem update => true, příznak pak odstranit
        $values = array_values(array_map(function (object $entry): array {
                unset($entry->update);
                return (array) $entry;
            }, array_filter((array) $content, fn(object $entry) => $entry->update ?? false)));

        if ($values) {
            $langColumns = array_map(fn(string $lang) => 'value_'.$lang, $this->langs);
            // column = VALUES(column) ...
            $valuesUpdate = array_map(fn(string $col) => sprintf('[%s] = VALUES([%s])', $col, $col), $langColumns);

            $this->connection->command()->insert()->into('content_locale', ...$values)
                ->on('DUPLICATE KEY UPDATE %sql', implode(',', $valuesUpdate))->execute();
        }
    }
}