<?php
declare(strict_types=1);

namespace App\Localization;

use Nette\Localization\Translator;

/**
 *
 *
 * @author Michal Kvita <michal.kvita@viaaurea.cz>
 * @copyright Copyright Via Aurea s. r. o.
 */
trait TranslatorAwareTrait
{
    private Translator $translator;

    public function getTranslator(): Translator
    {
        return $this->translator;
    }

    public function setTranslator(Translator $translator)
    {
        $this->translator = $translator;
        return $this;
    }

    protected function _(...$args): string
    {
        return $this->translator->translate(...$args);
    }
}