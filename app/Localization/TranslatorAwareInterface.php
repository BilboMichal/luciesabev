<?php
declare(strict_types=1);

namespace App\Localization;

use Nette\Localization\Translator;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
interface TranslatorAwareInterface
{

    function getTranslator(): Translator;

    function setTranslator(Translator $translator);
}