<?php
declare(strict_types=1);

namespace App;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class Env
{

    public static function get(string $name, string $default = null)
    {
        return $_ENV[$name] ?? $default;
    }
}