<?php
declare(strict_types=1);

namespace App\Mailing;

use App\Localization\TranslatorAwareTrait;
use App\Localization\TranslatorAwareInterface;
use Nette\Mail\Mailer;
use Nette\Mail\Message;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class ContactMailer implements TranslatorAwareInterface
{

    use TranslatorAwareTrait;

    public function __construct(private Mailer $mailer)
    {

    }

    public function sendConfirmationMail(array $values)
    {
        $email = new Message;
        $email->setFrom($values['from'], $values['name'] ?? null);
        $email->addTo($values['to']);
        $email->setSubject($this->_('Vaše zpráva byla úspěšně odeslána'));
        $email->setBody($values['message']);

        $this->mailer->send($email);
    }

    public function sendMessageMail(array $values)
    {
        $email = new Message;
        $email->setFrom($values['from'], $values['name'] ?? null);
//        $email->addTo($values['to']);
        $email->addTo('Mikvt@seznam.cz');
        $email->setSubject('Kontakt blablabla');
        $email->setBody($values['message']);

        $this->mailer->send($email);
    }
}