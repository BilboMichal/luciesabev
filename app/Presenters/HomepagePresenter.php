<?php
declare(strict_types=1);

namespace App\Presenters;

use Nette\Application\BadRequestException;
use Nette\Iterators\Mapper;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class HomepagePresenter extends BasePresenter
{

    public function renderCategory(int $id = null, string $urltitle = null): void
    {
        if ($this->isAjax()) {
            $this->redrawControl('content');
            $this->redrawControl('nav-menu');
        }

        if ($id !== null) {
            $category = $this->connection->select('*')
                ->from('categories')
                ->where('id = %i', $id)
                ->fetch();
            if (!$category) {
                throw new BadRequestException(sprintf('Category ID %d does not exist', $id));
            }

            // Redirect to canonical URLtitle...
            $canonicalUrlTitle = self::genUrlTitle($category->{'title_'.$this->lang});
            if ($urltitle !== $canonicalUrlTitle) {
                $this->redirect('this', [$id, $canonicalUrlTitle]);
            }

            // Generating thumbs...
            /* foreach ($categoryImages as $categoryImage) {
              $categoryImageObj = Image::fromFile($categoryImagesDir.'/'.$categoryImage);
              $categoryImageObj->scale(1000)->save($categoryImagesDir.'/thumb/'.$categoryImage);
              } */

            $categoryImagesSubPath = sprintf('/img/categories/%03d', $id);
            $categoryImagesDir = $this->appParameters['wwwDir'].$categoryImagesSubPath;

            // Category template data...
            $this->template->category = (object) [
                    'id' => $category->id,
                    'title' => $category->{'title_'.$this->lang},
                    'description' => $category->{'description_'.$this->lang},
                    // Category images template data...
                    'images' => new Mapper($this->connection->select('*')
                            ->from('images')
                            ->where('category = %i', $id),
                        function (object $imageRow) use ($categoryImagesSubPath): object {
                            return (object) [
                                'link' => $this->template->basePath.$categoryImagesSubPath.'/'.$imageRow->filename,
                                'thumb' => $this->template->basePath.$categoryImagesSubPath.'/thumb/'.$imageRow->filename,
                                'title' => $imageRow->{'title_'.$this->lang},
                            ];
                        }),
            ];
        }
    }
}