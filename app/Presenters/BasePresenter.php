<?php
declare(strict_types=1);

namespace App\Presenters;

use App\AppParametersTrait;
use App\Forms\FormFactory;
use App\Localization\TranslatorAwareInterface;
use App\Localization\TranslatorAwareTrait;
use App\Mailing\ContactMailer;
use Dibi\Connection as DibiConnection;
use Nette\Application\UI\Form;
use Nette\Application\UI\Presenter as NetteUIPresenter;
use Nette\InvalidStateException;
use Nette\Iterators\Mapper;
use Nette\Utils\Strings;
use stdClass;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
abstract class BasePresenter extends NetteUIPresenter implements TranslatorAwareInterface
{

    use AppParametersTrait;
    use TranslatorAwareTrait;
    /** @persistent */
    public ?string $lang;
    private object $content;

    // ------------------------------------------------------------------------------
    // get/set

    public function __construct(protected DibiConnection $connection, protected FormFactory $formFactory,
                                protected ContactMailer $contactMailer)
    {

    }

    public function getLocale(): array
    {
        if (!isset($this->appParameters)) {
            throw new InvalidStateException('$appParameters was not set');
        }
        
        return $this->appParameters['locale'];
    }

    public function getContent(): object
    {
        if (!isset($this->content)) {
            $this->content = (object) $this->connection->select('[key], [value]')->from('content')->fetchPairs();
        }

        return $this->content;
    }

    // ------------------------------------------------------------------------------
    // override

    public function flashMessage($message, $type = 'info'): stdClass
    {
        $message = $this->getTranslator()->translate($message);
        return parent::flashMessage($message, $type);
    }

    // ------------------------------------------------------------------------------
    // runtime

    protected static function genUrlTitle(string $title): string
    {
        return Strings::webalize($title);
    }

    protected function startup(): void
    {
        parent::startup();

        // Localization
        // ------------------------------------------------------------

        $locale = $this->getLocale();
        $langs = $locale['langs'];

        // Redirect if non-existent lang
        if (isset($this->lang) && !isset($langs[$this->lang])) {
            $this->lang = null;
            $this->redirect('this');
        }
        // Set default lang if none
        if (!isset($this->lang)) {
            $this->lang = $locale['defaultLang'];
        }
        // Lang for translation
        if (isset($this->translator)) {
            $this->translator->setLang($this->lang);
        }
    }

    public function handleLang(string $lang)
    {
        $this->lang = $lang;
        $this->redirect('this');
    }

    public function beforeRender(): void
    {
        parent::beforeRender();

        $categories = $this->connection->select('*')
            ->from('categories');

        $this->template->content = $this->getContent();
        $this->template->locale = $this->getLocale();
        $this->template->lang = $this->lang;
        $this->template->homeTarget = $this->lazyLink('Homepage:category', [null]);

        $this->template->categorySlides = new Mapper($categories,
            function (object $category): object {
                $categoryTitle = $category->{'title_'.$this->lang};
                $imageMask = $this->template->basePath.'/img/categories/%s/%03d.jpg';
                return (object) [
                    'id' => $category->id,
                    'title' => $categoryTitle,
                    'target' => $this->lazyLink('Homepage:category', [$category->id, self::genUrlTitle($categoryTitle)]),
                    'imageLarge' => sprintf($imageMask, 'large', $category->id),
                    'imageSmall' => sprintf($imageMask, 'small', $category->id),
                    'description' => $category->{'description_'.$this->lang},
                ];
            });

        $this->template->addFilter('switchLang',
            fn(string $lang): string => $this->link('this', ['lang' => $lang] + $this->getParameters()));
    }

    protected function createComponentContactForm(): Form
    {
        $requiredMsg = 'Toto pole je povinné';
        $emailMsg = 'Je nutné zadat korektní e-mail adresu';

        $form = $this->formFactory->create();
        $form->addText('name', 'Jméno a příjmení')
            ->setRequired($requiredMsg);
        $form->addText('email', 'E-mail')
            ->setRequired($requiredMsg)
            ->addRule(Form::EMAIL, $emailMsg);
        $form->addText('phone', 'Telefon');
        $form->addTextArea('message', 'Zpráva')
            ->setRequired($requiredMsg);
        $form->addSubmit('send', 'Odeslat');

        $form->onSuccess[] = function (Form $form) {
            $formValues = $form->getValues();
            $content = $this->getContent();
            $sent = $this->contactMailer->sendMessageMail([
                'from' => $formValues->email,
                'to' => $content->email,
                'name' => $formValues->name,
                'message' => $formValues->message,
            ]);
            $this->flashMessage('Zpráva odeslána');
            $this->redirect('this');
        };

        return $form;
    }
}