<?php
declare(strict_types=1);

namespace App\Factories;

use Nette\Mail\Mailer;
use Nette\Mail\SendmailMailer;
use Nette\Mail\SmtpMailer;

/**
 *
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class MailerFactory
{

    public static function create(string $dsn = null): Mailer
    {
        $config = isset($dsn) ? parse_url($dsn) : [];

        if (isset($config['scheme'])) {
            if (str_starts_with($config['scheme'], 'smtp')) {
                $mailer = new SmtpMailer([
                    'host' => $config['host'],
                    'port' => $config['port'] ?? null,
                    'username' => $config['user'] ?? null,
                    'password' => $config['pass'] ?? null,
                    'secure' => $config['scheme'] === 'smtps' ? 'ssl' : null,
                ]);
            }
        }

        // default mailer
        if (!isset($mailer)) {
            $mailer = new SendmailMailer();
        }

        return $mailer;
    }
}