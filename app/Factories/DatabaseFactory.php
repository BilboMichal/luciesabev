<?php
declare(strict_types=1);

namespace App\Factories;

use Dibi\Bridges\Tracy\Panel as DibiTracyPanel;
use Dibi\Connection as DibiConnection;

/**
 *
 * @author Michal Kvita <Mikvt@seznam.cz>
 */
final class DatabaseFactory
{

    public static function create(string $dsn, bool $debug = false): DibiConnection
    {
        $config = parse_url($dsn);

        $connection = new DibiConnection([
            'host' => $config['host'],
            'username' => $config['user'],
            'password' => $config['pass'],
            'database' => ltrim($config['path'], '/'),
            'lazy' => true,
        ]);

        if ($debug) {
            (new DibiTracyPanel)->register($connection);
        }

        return $connection;
    }
}