function App(options) {
    this.options = $.extend(true, {}, this.defaults, options);
}

App.prototype = {
    loader: null,

    /**
     * Inialization of JS components logic
     *
     * @returns {void}
     */
    init: function () {
        const self = this;

        // Loader
        setTimeout(function () {
            $('.content-loader').fadeOut(800, function () {
                $(this).remove();
            });
        }, 400);

        $.nette.init();
    },

    initPromoSlider: function (element) {
        element.slick({
            dots: true,
            autoplay: true,
            cssEase: 'linear',
            infinite: true,
            pauseOnHover: true,
            adaptiveHeight: true,
            /*draggable: true,
             swipeToSlide: true,*/
            fade: true,
            centerMode: true,
        });

        return element;
    },

    initImagesGrid: function (element, lang) {
        const fancybox = $(element).fancybox({
            buttons: [
                'download',
                'thumbs',
                'slideShow',
                'close'
            ],
            animationEffect: 'zoom-in-out',
            lang: lang,
            i18n: {
                en: {
                    CLOSE: "Close",
                    NEXT: "Next",
                    PREV: "Previous",
                    ERROR: "The requested content cannot be loaded. <br/> Please try again later.",
                    PLAY_START: "Start slideshow",
                    PLAY_STOP: "Pause slideshow",
                    FULL_SCREEN: "Full screen",
                    THUMBS: "Thumbnails",
                    DOWNLOAD: "Download",
                    SHARE: "Share",
                    ZOOM: "Zoom"
                },
                cs: {
                    CLOSE: "Zavřít",
                    NEXT: "Další",
                    PREV: "Předchozí",
                    ERROR: "Požadovaný obsah nelze načíst. <br /> Zkuste prosím znovu za chvíli.",
                    PLAY_START: "Spustit prezentaci",
                    PLAY_STOP: "Pozastavit prezentaci",
                    FULL_SCREEN: "Celá obrazovka",
                    THUMBS: "Mřížka",
                    DOWNLOAD: "Stáhnout",
                    SHARE: "Sdílet",
                    ZOOM: "Přiblížit"
                }
            }
        });

        return element;
    },
}

const app = new App;
$(function () {
    "use strict";
    app.init();
});
