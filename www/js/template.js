(function ($) {
    "use strict";

    var TEMPLATE = window.TEMPLATE || {};

    /*-------------------------------------------------------------------*/
    /*      Replace each select with a custom dropdown menu
     /*-------------------------------------------------------------------*/

    TEMPLATE.selectReplacer = function () {

        $('select').each(function () {
            var $select = $(this),
                $ul = $('<ul></ul>').addClass('select-replacer'),
                $hiddenInput = $('<input type="hidden" name="' + $select.attr('name') + '" value="' + $select.val() + '">');

            $select.after($ul);
            $ul.after($hiddenInput);

            $select.children('option').each(function () {
                var $that = $(this),
                    $li = $('<li data-value="' + $that.val() + '">' + $that.text() + '</li>');
                if ($that.attr('class') != undefined) {
                    $li.addClass($that.attr('class'));
                }
                $ul.append($li);
            });

            $ul.children('li').not(':first').hide();

            $ul.children('li').on('click', function () {
                var $clickedLi = $(this),
                    dataValue = $clickedLi.data('value');
                $clickedLi.prependTo($ul.toggleClass('open')).nextAll().toggle();
                $hiddenInput.val(dataValue);
                $('.hidden-field').removeClass('show').find('input').removeClass('required');
                $('#' + $clickedLi.attr('class')).addClass('show').find('input').addClass('required');
            });

            $select.remove();

            //close the list by clicking outside of it
            $(document).on('click', function (e) {

                if (!$('form label').find(e.target).length) {
                    $ul.removeClass('open').children('li').not(':first').hide();
                }

            });

        });

    },
        /*-------------------------------------------------------------------*/
        /*      Toggle
         /*-------------------------------------------------------------------*/

        TEMPLATE.toggle = function () {

            $('.open .content-toggle').show();
            $('.title-toggle').on('click', function (e) {
                e.preventDefault();

                var $that = $(this),
                    $toggle = $that.parent(),
                    $contentToggle = $that.next(),
                    $accordion = $that.parents('.accordion');

                if ($accordion.length > 0) {
                    $accordion.find('.content-toggle').slideUp('normal', function () {
                        $(this).parent().removeClass('open');
                    });
                    if ($that.next().is(':hidden')) {
                        $contentToggle.slideDown('normal', function () {
                            $toggle.addClass('open');
                        });
                    }
                } else {
                    $contentToggle.slideToggle('normal', function () {
                        $toggle.toggleClass('open');
                    });
                }
            });

        },
        /*-------------------------------------------------------------------*/
        /*      Tabs
         /*-------------------------------------------------------------------*/

        TEMPLATE.tabs = function () {

            $('.title-tab:first-child').addClass('selected-tab');
            $('.title-tab').on('click', function (e) {
                e.preventDefault();

                var $that = $(this),
                    $tabParent = $that.parents('.tabs'),
                    idTab = $that.find('a').attr('href');

                if (!$that.hasClass('selected-tab')) {
                    $tabParent.find('.tab').hide().removeClass('open');
                    $tabParent.find('.title-tab').removeClass('selected-tab');
                    $that.addClass('selected-tab');
                    $(idTab).fadeIn().addClass('open');
                }

            });

        },
        /*-------------------------------------------------------------------*/
        /*      Portfolio Layout
         /*-------------------------------------------------------------------*/

        TEMPLATE.portfolio = {
            init: function () {
                this.layout();
                this.filters();
                this.infoItems();
            },

            // build the portfolio layout
            layout: function () {
                $('.works').imagesLoaded(function () {
                    $('.works').isotope();
                });
            },

            // filter items on button click
            filters: function () {
                $('.filters').on('click', 'a', function (e) {
                    e.preventDefault();

                    var $that = $(this),
                        filterValue = $that.attr('data-filter');

                    $('.filters a').removeClass('light');
                    $that.addClass('light');
                    $('.works').isotope({filter: filterValue});
                });
            },

            // open/close portfolio item information
            infoItems: function () {
                $('.info-link').on('click', function (e) {
                    e.preventDefault();

                    var $that = $(this),
                        $extraItem = $that.parents('.work-thumb').next('.info-work');

                    if ($extraItem.length > 0) {
                        $extraItem.slideToggle(200, function () {
                            $(this).parents('.work').toggleClass('opened');
                            $('.works').isotope('layout');
                        });
                    }

                });
            }

        },
        /*-------------------------------------------------------------------*/
        /*      Scroll to Section (One Page Version)
         /*-------------------------------------------------------------------*/

        TEMPLATE.scrollToSection = function () {
            $('.one-page #nav-menu a[href^="#"]').on('click', function (e) {
                e.preventDefault();

                var target = this.hash,
                    $section = $(target);

                $(this).parent().addClass('selected');
                $('html, body').stop().animate({
                    scrollTop: $section.offset().top - 79
                }, 900, 'swing', function () {
                    window.location.hash = target.replace(/^#/, '#!');
                });
                $('body').removeClass('open');
                $('#nav-menu').find('li').removeClass('show');

            });

        },
        /*-------------------------------------------------------------------*/
        /*      Highlight Navigation Link When Scrolling (One Page Version)
         /*-------------------------------------------------------------------*/

        TEMPLATE.scrollHighlight = function () {

            var scrollPosition = $(window).scrollTop();

            if ($('body').hasClass('one-page')) {

                if (scrollPosition >= 200) {

                    $('.section').each(function () {

                        var $link = $('#nav-menu a[href="#' + $(this).attr('id') + '"');
                        if ($link.length && $(this).position().top <= scrollPosition + 80) {
                            $('#nav-menu li').removeClass('selected');
                            $link.parent().addClass('selected');
                        }
                    });

                } else {

                    $('#nav-menu li').removeClass('selected');

                }
            }

        },
        /*-------------------------------------------------------------------*/
        /*      Mobile Menu
         /*-------------------------------------------------------------------*/

        TEMPLATE.mobileMenu = {

            init: function () {

                this.toggleMenu();
                this.addClassParent();
                this.addRemoveClasses();

            },

            // toggle mobile menu
            toggleMenu: function () {
                var self = this,
                    $body = $('body');

                $('#nav-toggle').click(function (e) {
                    e.preventDefault();

                    if ($body.hasClass('open')) {
                        $body.removeClass('open');
                        $('#nav-menu').find('li').removeClass('show');
                    } else {
                        $body.addClass('open');
                        self.showSubmenu();
                    }

                });

            },

            // add 'parent' class if a list item contains another list
            addClassParent: function () {
                $('#nav-menu').find('li > ul').each(function () {
                    $(this).parent().addClass('parent');
                });
            },

            // add/remove classes to a certain window width
            addRemoveClasses: function () {
                var $nav = $('#nav-menu');

                if ($(window).width() < 992) {
                    $nav.addClass('mobile');
                } else {
                    $('body').removeClass('open');
                    $nav.removeClass('mobile').find('li').removeClass('show');
                }
            },

            // show sub menu
            showSubmenu: function () {
                $('#nav-menu').find('a').each(function () {
                    var $that = $(this);
                    if ($that.next('ul').length) {
                        $that.one('click', function (e) {
                            e.preventDefault();
                            $(this).parent().addClass('show');
                        });
                    }

                });

            }

        },
        /*-------------------------------------------------------------------*/
        /*      Sticky Menu
         /*-------------------------------------------------------------------*/

        TEMPLATE.stickyMenu = function () {

            if ($(window).scrollTop() > 50) {
                $('body').addClass('sticky');
            } else {
                $('body').removeClass('sticky');

            }

        },
        /*-------------------------------------------------------------------*/
        /*      Show/Hide Bottom Contacts Bar
         /*-------------------------------------------------------------------*/

        TEMPLATE.contactsBar = function () {

            if ($(window).scrollTop() + $(window).height() > $('footer').offset().top) {
                $('#contacts-bar').fadeOut('fast');
            } else {
                $('#contacts-bar').fadeIn('fast');
            }

        },
        /*-------------------------------------------------------------------*/
        /*      Custom Backgrounds
         /*-------------------------------------------------------------------*/

        TEMPLATE.backgrounds = function () {

            $.each(config.backgrouns, function (key, value) {

                var $el = $(key),
                    $overlay = $('<div class="bg-overlay"></div>');

                if (value.img != null) {
                    $el.addClass('bg').css('background-image', 'url(' + value.img + ')').prepend($overlay);
                }

                if (value.overlay != null && !value.disableOverlay) {
                    $el.find('.bg-overlay').remove();
                }

                if (value.overlayOpacity != null) {
                    $el.find('.bg-overlay').css('opacity', value.overlayOpacity);
                }

                if (value.overlayColor != null) {
                    $el.find('.bg-overlay').css('background-color', value.overlayColor);
                }

                if (value.pattern != null && value.pattern) {
                    $el.addClass('pattern');
                }

                if (value.position != null) {
                    $el.css('background-position', value.position);
                }

                if (value.bgCover != null) {
                    $el.css('background-size', value.bgCover);
                }

                if (value.parallax != null && value.parallax) {
                    $el.addClass('plx');
                }

            });

        },
        /*-------------------------------------------------------------------*/
        /*      Parallax
         /*-------------------------------------------------------------------*/

        TEMPLATE.parallax = function () {

            $('.plx').each(function () {
                $(this).parallax('50%', 0.5);
            });

        },
        /*-------------------------------------------------------------------------------------------------*/
        /*      If url has #SECTION_NAME parameter then scroll to relative section after page loading
         /*-------------------------------------------------------------------------------------------------*/

        TEMPLATE.goToSection = function () {
            var hash = window.location.hash.replace(/^#!/, '#');
            $('.one-page #nav-menu a[href="' + hash + '"]').trigger('click');
        };

    /*-------------------------------------------------------------------*/
    /*      Initialize all functions
     /*-------------------------------------------------------------------*/

    $(document).ready(function () {
        TEMPLATE.selectReplacer();
        TEMPLATE.toggle();
        TEMPLATE.tabs();
        TEMPLATE.portfolio.init();
        TEMPLATE.scrollToSection();
        TEMPLATE.mobileMenu.init();
        TEMPLATE.backgrounds();
        TEMPLATE.parallax();

    });

    // window resize scripts
    $(window).resize(function () {
        TEMPLATE.portfolio.layout();
        TEMPLATE.mobileMenu.addRemoveClasses();
    });

    // window scroll scripts
    $(window).scroll(function () {

        TEMPLATE.stickyMenu();
        TEMPLATE.scrollHighlight();
        TEMPLATE.contactsBar();

    });

})(jQuery);