<?php
declare(strict_types=1);

use App\Bootstrap;
use App\Localization\Translator;
use Nette\Application\Application as NetteApplication;

require __DIR__.'/../vendor/autoload.php';

$configurator = Bootstrap::boot();
$container = $configurator->createContainer();

$debugMode = $container->parameters['debugMode'];
/* @var $application NetteApplication */
$application = $container->getByType(NetteApplication::class);
$application->run();

// Aktualizace překladů do DB
/* @var $translator Translator */
$translator = $container->getByType(Translator::class);
$translator->updateTranslations();
